# Spring Security

Spring Security is a powerful and highly customizable authentication and access-control framework. It is the de-facto standard for securing Spring-based applications.

[Offical Documentation](https://docs.spring.io/spring-security/site/docs/5.0.0.BUILD-SNAPSHOT/reference/htmlsingle/)

[Core Components](https://docs.spring.io/spring-security/site/docs/3.0.x/reference/technical-overview.html) : <br/>
SecurityContextHolder, SecurityContext and Authentication Objects, UserDetailsService, UserDetails, ...



# JWT

Wiki [JWT](https://www.wikiwand.com/en/JSON_Web_Token)

JSON Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties.

Tools:<br/>
* [jwt.io](https://jwt.io/)
* [JJWT](https://github.com/jwtk/jjwt)



# Spring Security + JWT 

refs:<br>

* [http://technicalrex.com/2015/02/20/stateless-authentication-with-spring-security-and-jwt](http://technicalrex.com/2015/02/20/stateless-authentication-with-spring-security-and-jwt)
* [https://github.com/brahalla/Cerberus/blob/master/README.md](https://github.com/brahalla/Cerberus/blob/master/README.md)
* [https://github.com/Robbert1/boot-stateless-auth/blob/master/README.md](https://github.com/Robbert1/boot-stateless-auth/blob/master/README.md)
* [https://github.com/szerhusenBC/jwt-spring-security-demo/blob/master/README.md](https://github.com/szerhusenBC/jwt-spring-security-demo/blob/master/README.md)
* [https://www.jianshu.com/p/6307c89fe3fa](https://www.jianshu.com/p/6307c89fe3fa)


