package com.example.securityweb.service;

import com.example.securityweb.dto.UserDto;
import com.example.securityweb.entity.User;
import com.example.securityweb.request.RegisterRequest;

public interface AuthService {
    UserDto register(RegisterRequest RegisterRequest);
    String login(String username, String password);
    String refresh(String oldToken);
}
