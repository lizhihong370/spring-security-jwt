package com.example.securityweb.service;

import com.example.securityweb.dto.UserDto;
import com.example.securityweb.entity.Role;
import com.example.securityweb.entity.User;
import com.example.securityweb.repository.RoleRepository;
import com.example.securityweb.repository.UserRepository;
import com.example.securityweb.request.RegisterRequest;
import com.example.securityweb.security.JwtUserDetails;
import com.example.securityweb.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {

    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;
    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    public AuthServiceImpl(AuthenticationManager authenticationManager, UserDetailsService userDetailsService,
                           JwtTokenUtil jwtTokenUtil, UserRepository userRepository, RoleRepository roleRepository) {

        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDto register(RegisterRequest registerRequest) {

        System.out.println("==== registerRequest: "+registerRequest+" ===");

        List<Role> roleList = registerRequest.getRoles().stream().map(role_id -> roleRepository.findOne(role_id)).collect(Collectors.toList());

        final String username = registerRequest.getUsername();
        if (userRepository.findByUsername(username) != null) {
            return null;
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        final String rawPassword = registerRequest.getPassword();
        User userToAdd = new User(username, encoder.encode(rawPassword), registerRequest.getEmail(),
                registerRequest.getLastPasswordResetDate(), roleList);

        User user = userRepository.saveAndFlush(userToAdd);

        return new UserDto(user);
    }

    @Override
    public String login(String username, String password) {
        System.out.println("===login === username " + username + " === password " + password + " ===");
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = authenticationManager.authenticate(upToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        System.out.println("=== try login username " + username + " =====");
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return token;
    }

    @Override
    public String refresh(String oldToken) {
        final String token = oldToken.substring(tokenHead.length());
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUserDetails user = (JwtUserDetails) userDetailsService.loadUserByUsername(username);
        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            return jwtTokenUtil.refreshToken(token);
        }
        return null;
    }
}
