package com.example.securityweb.request;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RegisterRequest {

    private String username;

    private String password;

    private String email;

    private Date lastPasswordResetDate;

    private List<Integer> roles;
}
