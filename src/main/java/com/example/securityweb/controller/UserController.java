package com.example.securityweb.controller;

import com.example.securityweb.dto.UserDto;
import com.example.securityweb.entity.User;
import com.example.securityweb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@PreAuthorize("hasRole('ADMIN')")
public class UserController {
    @Autowired
    private UserRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDto> getUsers() {
        return repository.findAll().stream().map(UserDto::new).collect(Collectors.toList());
    }

}

