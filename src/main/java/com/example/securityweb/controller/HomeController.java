package com.example.securityweb.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class HomeController {

    @RequestMapping("/")
    public String home(){
        return "hi";
    }

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping("/secret")
    public String secret(){
        return "secret syu----";
    }

}
