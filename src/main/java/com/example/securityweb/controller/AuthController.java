package com.example.securityweb.controller;

import com.example.securityweb.dto.UserDto;
import com.example.securityweb.entity.User;
import com.example.securityweb.request.JwtAuthenticationRequest;
import com.example.securityweb.request.RegisterRequest;
import com.example.securityweb.response.JwtAuthenticationResponse;
import com.example.securityweb.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JwtAuthenticationResponse login(
            @RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {

        final String token = authService.login(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        return new JwtAuthenticationResponse(token);
    }

    @RequestMapping(value = "/refresh-token", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAuthToken(HttpServletRequest request) throws AuthenticationException {
        String currentToken = request.getHeader(tokenHeader);
        String refreshedToken = authService.refresh(currentToken);
        if (refreshedToken == null) {
            //是不是应该 throws AuthenticationException ？
            return ResponseEntity.badRequest().body(null);
        } else {
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public UserDto register(@RequestBody RegisterRequest registerRequest) throws AuthenticationException {
        return authService.register(registerRequest);
    }
}
