package com.example.securityweb.dto;

import com.example.securityweb.entity.Role;
import com.example.securityweb.entity.User;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class UserDto {
    int id;

    private String username;

    private String email;

    private Date lastPasswordResetDate;

    private List<String> roles;

    public UserDto(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.lastPasswordResetDate = user.getLastPasswordResetDate();
        this.username = user.getUsername();
        this.roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());
    }
}
