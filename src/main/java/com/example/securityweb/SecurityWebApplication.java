package com.example.securityweb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SecurityWebApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SecurityWebApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
//        String encode = new BCryptPasswordEncoder().encode("12345");
//        System.out.println("encode " + encode);
    }
}
