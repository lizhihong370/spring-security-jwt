package com.example.securityweb.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "role")
public class Role {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int id;

    @Column(name = "name")
    String name;

    @ManyToMany(mappedBy = "roles",fetch = FetchType.EAGER)
    List<User> users;
}
