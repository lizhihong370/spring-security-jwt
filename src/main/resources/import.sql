INSERT INTO role (id, name) VALUES (1, 'ADMIN');
INSERT INTO role (id, name) VALUES (2, 'USER');


INSERT INTO user (id, email,last_password_reset_date,password,username) VALUES (1,'lzh@qq.com','2017-12-01','$2a$10$yikRVmJRwqoyRETRJPsqlurN3WqnbBnuo6H7KHx61DD1H4x2hU.g6','lzh');
INSERT INTO user (id, email,last_password_reset_date,password,username) VALUES (2,'user@qq.com','2018-01-01','$2a$10$yikRVmJRwqoyRETRJPsqlurN3WqnbBnuo6H7KHx61DD1H4x2hU.g6','pop');

INSERT INTO user_role (user_id,role_id) VALUES (1, 1);
INSERT INTO user_role (user_id,role_id) VALUES (1, 2);
INSERT INTO user_role (user_id,role_id) VALUES (2, 2);
